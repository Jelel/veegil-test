
import React, { Component } from 'react';
import SignUp from './component/SignUp';
import Login from './component/Login';
import Deposit from './component/deposit/Deposit';
import Payout from './component/payout/Payout';
import NavBar from './containers/navigation/Navigation';
import './App.css';
import 'tachyons';


class App extends Component {

	state = {
		route: 'signin',
		isSignedIn: false,
		operation: '',
		user: {
			surname: '',
			acctNo: '',
			transactions: null,
			payouts: [],
			deposits: [],
			joined: ''
		}
	}

	operationSelector = (deposit) => {
		this.setState({operation: deposit});
	}

	onRouteChange = (route) => {
		if (route === 'signout') {
			this.setState({ isSignedIn: false});
		} else if (route === 'home') {
			this.setState({isSignedIn: true})
		}

		this.setState({route: route});
	}

	loadUsers = (data) => {
		this.setState({user: {
			surname: data.surname,
			acctNo: data.acctNo,
			password: data.password,
			transactions: data.transactions,
			payouts: data.payouts,
			deposits: data.deposits,
			joined: data.joined
		}})
	}

	authMethodHandler = (method) => {
		this.setState({authMethod: method})
	}
	
	render () {
		const { route, isSignedIn, operation, authMethod, user: {acctNo, surname} } = this.state;
		console.log(acctNo);
		return (
			<div className='App'>
				<NavBar surname={surname} onRouteChange={this.onRouteChange} authMethod={authMethod} isSignedIn={isSignedIn} operationSelector={this.operationSelector} />
				{route === 'home'
						? ( operation === 'deposit'
								? <Deposit acctNo={acctNo} />
								: ( operation === 'payout'
										? <Payout acctNo={acctNo}/>
										: null
									)
						  )
												
						: ( this.state.route === 'signin'
								? <Login
												 loadUsers={this.loadUsers}
												 onRouteChange={this.onRouteChange}
											 />
								: <SignUp loadUsers={this.loadUsers} 
													onRouteChange={this.onRouteChange}
												/>
							)
				}
			</div>
		)
	}
}

export default App;
