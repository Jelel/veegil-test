import React from 'react'
import './Navigation.css'

const Navigation = ({onRouteChange, isSignedIn, operationSelector, surname, menu}) => {
	return ( 
		<div>
			{ isSignedIn === true 
				? <div>
						<nav>
							<p 
							onClick={() => operationSelector('no')}
							className='f3 link dim black underline pr3 pointer'>Home</p>
							<p 
							onClick={() => operationSelector('deposit')}
							className='f3 link dim black underline pr3 pointer'>Deposit</p>
							<p 
							onClick={() => operationSelector('payout')}
							className='f3 link dim black underline pr3 pointer'>Payout</p>
							<p 
							onClick={() => onRouteChange('signout')}
							className='f3 link dim black underline pr3 pointer'>Sign Out</p>
						</nav>
						<h1 className='greet'>Hi <span>{surname}, </span>Welcome to Veegil Banking!!!</h1>
					</div>
				: <nav>
						<p 
						 onClick={() => onRouteChange('signin')} 
						 className='f3 link dim black underline pr3 pointer'>Login</p>
						<p 
						 onClick={() => onRouteChange('register')} 
						 className='f3 link dim black underline pr3 pointer'>Sign up</p>
					</nav>				
			}
		</div>
	)
}

export default Navigation;
