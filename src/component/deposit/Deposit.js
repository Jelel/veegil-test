import React, { Component } from 'react';

import './Deposit.css';

class Deposit extends Component {
	state = {
			amount: null,
			deposits: [],
	}

	onAmountChange = (event) => {
		console.log(this.state.amount);
		this.setState({ amount: event.target.value })
	}
	
	onSubmitDeposit = () => {
		const { amount } = this.state;
		const { acctNo } = this.props;
		if (amount) {
			console.log(this.state.amount);
			fetch('https://cryptic-tundra-59181.herokuapp.com/deposit', {
				method: 'post',
				headers: {'content-Type': 'application/json'},
				body: JSON.stringify({
					amount: amount,
					acctNo: acctNo
				})
			})
				.then(response => response.json())
				.then(user => {
					if(user.acctNo) {
						console.log(user.deposits);
						this.setState({deposits: user.deposits})
					} else {
						alert('Error Making Deposit');
					}
				})
		} else {
			alert('Amount Cannot be Empty');
		}
	}

	displayDeposits = () => {
 
	}


	render () {
			return (
				<div>
					<div className="deposit">
							<h1>Make a Deposit</h1>
							<label>Amount (NGN)</label>
							<input className="b mw5 pa2 input-reset ba bg-transparent hover-bg-black hover-white w-100" 
											type="number" 
											required 
											onChange={this.onAmountChange} />
							<button className='link dim black pointer' onClick={this.onSubmitDeposit}>Send</button>
					</div>
					<h1 className="heading">Deposits</h1>
					<div className='display'>
						
							{this.state.deposits.reverse().map(deposit => {
								return (
									<div className='info'>
										<div key={deposit.date} className='group'>
											<h3 className='category tc'>Deposited</h3>
											<h4 className='amount'>Amount:<span> {deposit.amount}</span></h4>
											<p className='date'>Date: {deposit.date}</p>
										</div>
									</div>
								)
							})}
				
					</div>
				</div>
			);
	}
}

export default Deposit;