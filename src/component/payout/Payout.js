import React, { Component } from 'react';

import './Payout.css';

class Payout extends Component {
	state = {
			amount: null,
			receiverSurname: null,
			receiverAcctNo: null,
			payouts: [],

	}

	// componentDidMount () {
	// 	fetch('/')
	// }

	onAmountChange = (event) => {
		this.setState({ amount: event.target.value })
	}

	onSurnameChange = (event) => {
		this.setState({ receiverSurname: event.target.value })
	}

	onAcctNoChange = (event) => {
		this.setState({ receiverAcctNo: event.target.value })
	}

	
	onSubmitPayout = () => {
		const { amount, receiverAcctNo, receiverSurname, payouts } = this.state;
		const { acctNo } = this.props;
		console.log(acctNo);
		console.log(this.state);
		if (amount && receiverAcctNo && receiverSurname) {
			console.log(this.state);
			fetch('https://cryptic-tundra-59181.herokuapp.com/payout', {
				method: 'post',
				headers: {'content-Type': 'application/json'},
				body: JSON.stringify({
					acctNo: acctNo,
					surname: receiverSurname,
					amount: amount,
					receiverAcctNo: receiverAcctNo
				})
			})
				.then(response => response.json())
				.then(user => {
					if(user.acctNo) {
						console.log(user.payouts);
						this.setState({payouts: user.payouts})
					} else {
						alert('Error Making Payment');
					}
				})
		} else {
			alert('Please, Fill All Fields');
		}
	}


	render () {
			return (
				<div>
					<div className="payout">
							<h1>Make a Payment</h1>
							<label>Recipent's Surname (NGN)</label>
							<input className="b mw5 pa2 input-reset ba bg-transparent hover-bg-black hover-white w-100" 
											type="text" 
											required 
											onChange={this.onSurnameChange} />
							<label>Recipent Account (Phone) No</label>
							<input className="b mw5 pa2 input-reset ba bg-transparent hover-bg-black hover-white w-100" 
											type="number" 
											required 
											onChange={this.onAcctNoChange} />
							<label>Amount (NGN)</label>
							<input className="b mw5 pa2 input-reset ba bg-transparent hover-bg-black hover-white w-100" 
											type="number" 
											required 
											onChange={this.onAmountChange} />
							<button className='link dim black pointer' onClick={this.onSubmitPayout}>Send</button>
					</div>
					<h1 className="heading">Payouts</h1>

					<div className='display'>
						
							{this.state.payouts.reverse().map(payout => {
								return (
									<div className='info'>
										<div key={payout.key} className='group'>
											<h3 className='category tc'>Payments</h3>
											<p className='category'>Surname: {payout.surname}</p>
											<p className='category'>Receiver's Account: {payout.account}</p>
											<h4 className='amount'>Amount:<span> {payout.amount}</span></h4>
											<p className='date'>Date: {payout.date}</p>
										</div>
									</div>
								)
							})}
				
					</div>
				</div>
			);
	}
}

export default Payout;