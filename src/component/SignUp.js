import React from 'react';

class SignUp extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			surname: '',
			acctNo: '',
			password: '',
			passwordMatch: ''
			
		}
	}

	onSurnameChange = (event) => {
		this.setState({ surname: event.target.value })
	}

	onAcctNoChange = (event) => {
		this.setState({ acctNo: event.target.value})
	}

	onPasswordChange = (event) => {
		this.setState({ password: event.target.value })
	}

	onPasswordMatchChange = (event) => {
		this.setState({ passwordMatch: event.target.value })
	}

	onSubmitSignUp = (event) => {
	  event.preventDefault();
		const { surname, acctNo, password, passwordMatch} = this.state;
		const { loadUsers, onRouteChange } = this.props;

		if ((surname && acctNo && password && passwordMatch) !== '' ) {
			if (password === passwordMatch) {
			fetch('https://cryptic-tundra-59181.herokuapp.com/signup', {
				method: 'post',
				headers: {'content-Type': 'application/json'},
				body: JSON.stringify({
					surname: surname,
					acctNo: acctNo,
					password: password,
				})
			})
				.then(response => response.json())
				.then(user => {
					if (user.acctNo) {
						loadUsers(user);
						onRouteChange('home');
					} else {
						alert("Invalid Account No and Passwood Combination");
					}
				})

			} else {
				alert("Error! Passwords Must Match");
			}

		}	else {
			alert('Please, Fill All Form Fields');
		}
	}

	render() {
		return (
			<div>
				<form className="tc br3 ba dark-gray b--black-10 mv4 w-100 w-50-m w-25-l mw6 shadow-5 center">
					<main className="pa4 black-80">
							<div className="measure">
								<fieldset id="sign_up" className="ba b--transparent ph0 mh0">
									<legend className="f1 fw6 ph0 mh0">Sign up</legend>
									<div className="mt3">
										<label className="db fw6 lh-copy f6" htmlFor="name">Surname</label>
										<input 
											className="pa2 mw5 input-reset ba bg-transparent hover-bg-black hover-white w-100"
											onChange={this.onSurnameChange} 
											type="text" 
											name="surname"  
											id="surname"
											required
										/>
									</div>
									<div className="mv3">
										<label className="db fw6 lh-copy f6" htmlFor="acctNo">Account (phone) No</label>
										<input 
											className="b pa2 mw5 input-reset ba bg-transparent hover-bg-black hover-white w-100" 
											onChange={this.onAcctNoChange}
											type="number"
											name="number"
											id="number"
											required
										/>
									</div>
									<div className="mt3">
										<label className="db fw6 lh-copy f6" htmlFor="password">Password</label>
										<input 
											className="pa2 mw5 input-reset ba bg-transparent hover-bg-black hover-white w-100"
											onChange={this.onPasswordChange}
											type="password" 
											name="password"  
											id="password"
											required
										/>
									</div>
									<div className="mt3">
										<label className="db fw6 lh-copy f6" htmlFor="passwordMatch">Re-type Password</label>
										<input 
											className="pa2 mw5 input-reset ba bg-transparent hover-bg-black hover-white w-100"
											onChange={this.onPasswordMatchChange}
											type="password" 
											name="passwordMatch"  
											id="passwordMatch"
											required
										/>
									</div>
								</fieldset>
								<div className="center">
									<input 
										onClick={this.onSubmitSignUp}
										className="b ph3 pv2 input-reset ba b--black bg-transparent grow pointer f6 dib pointer" 
										type="submit" 
										value="Sign up"
									/>
								</div>
							</div>
						</main>
					</form>
			</div>
		)
	}
}

export default SignUp;